<%@ include file="/WEB-INF/views/include.jsp" %>

<html>
  <head><title><fmt:message key="title"/></title></head>
     <body>
       <h1><fmt:message key="heading"/></h1>
       <p><fmt:message key="greeting"/> <c:out value="${model.now}"/></p>
       <h3>Products</h3>
       <c:forEach items="${model.players}" var="player">
         <c:out value="${player.name}"/> <i>$<c:out value="${player.steamId}"/></i><br><br>
       </c:forEach>
     </body>
</html>