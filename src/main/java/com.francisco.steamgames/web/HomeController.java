package com.francisco.steamgames.web;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.koraktor.steamcondenser.exceptions.SteamCondenserException;
import com.github.koraktor.steamcondenser.steam.SteamPlayer;
import com.github.koraktor.steamcondenser.steam.servers.GoldSrcServer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;




@Controller
public class HomeController {

    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value="/home.htm")
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        logger.info("Returning home view");

        GoldSrcServer server;
        Map players;
        int size;
        try {
            server = new GoldSrcServer("192.168.0.114", 27016);
            server.initialize();
            players = server.getPlayers();
        } catch (SteamCondenserException e) {
            players = null;
        } catch (TimeoutException e) {
            players = null;
        }
        size = players.size();

        Set<Entry<String, SteamPlayer>> entrySet = players.entrySet();
        for(Entry<String, SteamPlayer> e : entrySet ) {
            String key = e.getKey();
            SteamPlayer value = e.getValue();
            value.getSteamId();
        }


        String now = (new Date()).toString();
        System.out.println();
        logger.info("There are " + size + " players currently at " + now);

        Map<String, Object> myModel = new HashMap<String, Object>();
        myModel.put("now", now);
        myModel.put("players", players);
        myModel.put("size", size);
        return new ModelAndView("home", "model", myModel);
    }

}
