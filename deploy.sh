#!/bin/bash
set -x
#Create compiled .war
mvn package

#Delete deployed war and exploited subfolder
sudo rm -rf /opt/apache-tomcat-7.0.57/webapps/steamgames
rm -f /opt/apache-tomcat-7.0.57/webapps/steamgames.war

#Copy war to tomcat
cp target/steamgames.war /opt/apache-tomcat-7.0.57/webapps/

#Delete compiled files from project tree
mvn clean

